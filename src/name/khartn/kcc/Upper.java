package name.khartn.kcc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JEditorPane;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.cookies.EditorCookie;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@ActionID(category = "Edit",
id = "name.khartn.kcc.Upper")
@ActionRegistration(displayName = "#CTL_Upper")
@ActionReferences({
    @ActionReference(path = "Menu/Edit", position = 3650),
    @ActionReference(path = "Shortcuts", name = "O-F5 NUMPAD4")
})
@Messages("CTL_Upper=TO UPPER CASE")
public final class Upper implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
                Node[] n = TopComponent.getRegistry().getActivatedNodes();
        if (n.length == 1) {
            EditorCookie ec = (EditorCookie) n[0].getCookie(EditorCookie.class);
            if (ec != null) {
                JEditorPane[] panes = ec.getOpenedPanes();
                if (panes.length > 0) {
                    try {
                        if (panes[0].getSelectedText() != null) {
                            Integer position_s = panes[0].getSelectionStart();
                            Integer position_end = panes[0].getSelectionEnd();
                            panes[0].replaceSelection(panes[0].getSelectedText().toUpperCase());
                            panes[0].select(position_s, position_end);
                        }
                    } catch (Exception ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }
        }
    }
}
