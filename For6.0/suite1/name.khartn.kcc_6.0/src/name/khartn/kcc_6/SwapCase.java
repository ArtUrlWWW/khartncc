package name.khartn.kcc_6;

import javax.swing.JEditorPane;
import org.apache.commons.lang.WordUtils;
import org.openide.cookies.EditorCookie;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public final class SwapCase extends CookieAction
{
  protected void performAction(Node[] activatedNodes)
  {
    EditorCookie c = (EditorCookie)activatedNodes[0].getCookie(EditorCookie.class);
    if (c != null) {
      JEditorPane[] panes = c.getOpenedPanes();
      if (panes.length > 0) {
        String selection = panes[0].getSelectedText();
        int start = panes[0].getSelectionStart();
        int end = panes[0].getSelectionEnd();
        selection = WordUtils.swapCase(selection);
        panes[0].replaceSelection(selection);
        panes[0].select(start, end);
      }
    }
  }

  protected int mode() {
    return 8;
  }

  public String getName() {
    return NbBundle.getMessage(SwapCase.class, "CTL_SwapCase");
  }

  protected Class[] cookieClasses() {
    return new Class[] { EditorCookie.class };
  }

  protected void initialize()
  {
    super.initialize();

    putValue("noIconInMenu", Boolean.TRUE);
  }

  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  protected boolean asynchronous() {
    return false;
  }
}